// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vtictactoe__Syms.h"


//======================

void Vtictactoe::traceChg(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    Vtictactoe* t=(Vtictactoe*)userthis;
    Vtictactoe__Syms* __restrict vlSymsp = t->__VlSymsp;  // Setup global symbol table
    if (vlSymsp->getClearActivity()) {
	t->traceChgThis (vlSymsp, vcdp, code);
    }
}

//======================


void Vtictactoe::traceChgThis(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	if (VL_UNLIKELY((1U & (vlTOPp->__Vm_traceActivity 
			       | (vlTOPp->__Vm_traceActivity 
				  >> 2U))))) {
	    vlTOPp->traceChgThis__2(vlSymsp, vcdp, code);
	}
	if (VL_UNLIKELY((2U & vlTOPp->__Vm_traceActivity))) {
	    vlTOPp->traceChgThis__3(vlSymsp, vcdp, code);
	}
	if (VL_UNLIKELY((4U & vlTOPp->__Vm_traceActivity))) {
	    vlTOPp->traceChgThis__4(vlSymsp, vcdp, code);
	}
	if (VL_UNLIKELY((1U & ((vlTOPp->__Vm_traceActivity 
				>> 2U) | (vlTOPp->__Vm_traceActivity 
					  >> 3U))))) {
	    vlTOPp->traceChgThis__5(vlSymsp, vcdp, code);
	}
	if (VL_UNLIKELY((0x10U & vlTOPp->__Vm_traceActivity))) {
	    vlTOPp->traceChgThis__6(vlSymsp, vcdp, code);
	}
	vlTOPp->traceChgThis__7(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void Vtictactoe::traceChgThis__2(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+1,(vlTOPp->tictactoe__DOT__image_pixel),4);
	vcdp->chgBus  (c+2,(vlTOPp->tictactoe__DOT__image_pixel_x),4);
	vcdp->chgBus  (c+3,(vlTOPp->tictactoe__DOT__image_won),4);
	vcdp->chgBus  (c+4,(vlTOPp->tictactoe__DOT__numb__DOT__r_LFSR),4);
	vcdp->chgBus  (c+5,(((0x57e3U >= (IData)(vlTOPp->tictactoe__DOT__sign_address))
			      ? vlTOPp->tictactoe__DOT__data2__DOT__dat__DOT__rom_content
			     [vlTOPp->tictactoe__DOT__sign_address]
			      : 0U)),4);
	vcdp->chgBus  (c+6,(((0x57e3U >= (IData)(vlTOPp->tictactoe__DOT__sign_address))
			      ? vlTOPp->tictactoe__DOT__data2__DOT__dat2__DOT__rom_content
			     [vlTOPp->tictactoe__DOT__sign_address]
			      : 0U)),4);
	vcdp->chgBus  (c+7,(vlTOPp->tictactoe__DOT__numb__DOT__r_LFSR),4);
	vcdp->chgBit  (c+8,((1U & (((IData)(vlTOPp->tictactoe__DOT__numb__DOT__r_LFSR) 
				    >> 3U) ^ ~ ((IData)(vlTOPp->tictactoe__DOT__numb__DOT__r_LFSR) 
						>> 2U)))));
    }
}

void Vtictactoe::traceChgThis__3(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBit  (c+9,(vlTOPp->tictactoe__DOT__divider__DOT__state));
	vcdp->chgBus  (c+10,(vlTOPp->tictactoe__DOT__divider__DOT__counter),32);
    }
}

void Vtictactoe::traceChgThis__4(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+11,(vlTOPp->tictactoe__DOT__hcount),11);
	vcdp->chgBus  (c+12,(vlTOPp->tictactoe__DOT__vcount),10);
	vcdp->chgBus  (c+13,(vlTOPp->tictactoe__DOT__board_address),19);
	vcdp->chgBus  (c+14,(vlTOPp->tictactoe__DOT__sign_address),15);
	vcdp->chgBus  (c+15,(vlTOPp->tictactoe__DOT__win_address),15);
	vcdp->chgBus  (c+16,(vlTOPp->tictactoe__DOT__won_address),17);
	vcdp->chgBus  (c+17,(vlTOPp->tictactoe__DOT__won_word),17);
	vcdp->chgBus  (c+18,(vlTOPp->tictactoe__DOT__board_record[0]),15);
	vcdp->chgBus  (c+19,(vlTOPp->tictactoe__DOT__board_record[1]),15);
	vcdp->chgBus  (c+20,(vlTOPp->tictactoe__DOT__board_record[2]),15);
	vcdp->chgBus  (c+21,(vlTOPp->tictactoe__DOT__board_record[3]),15);
	vcdp->chgBus  (c+22,(vlTOPp->tictactoe__DOT__board_record[4]),15);
	vcdp->chgBus  (c+23,(vlTOPp->tictactoe__DOT__board_record[5]),15);
	vcdp->chgBus  (c+24,(vlTOPp->tictactoe__DOT__board_record[6]),15);
	vcdp->chgBus  (c+25,(vlTOPp->tictactoe__DOT__board_record[7]),15);
	vcdp->chgBus  (c+26,(vlTOPp->tictactoe__DOT__board_record[8]),15);
	vcdp->chgBus  (c+27,(vlTOPp->tictactoe__DOT__selector),2);
	vcdp->chgBus  (c+28,(vlTOPp->tictactoe__DOT__color_selector),8);
	vcdp->chgBus  (c+29,(vlTOPp->tictactoe__DOT__counter),27);
	vcdp->chgBit  (c+30,(vlTOPp->tictactoe__DOT__enable));
	vcdp->chgBit  (c+31,(vlTOPp->tictactoe__DOT__getRandom));
	vcdp->chgBit  (c+32,(vlTOPp->tictactoe__DOT__draw));
    }
}

void Vtictactoe::traceChgThis__5(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+33,(vlTOPp->tictactoe__DOT__board[0]),2);
	vcdp->chgBus  (c+34,(vlTOPp->tictactoe__DOT__board[1]),2);
	vcdp->chgBus  (c+35,(vlTOPp->tictactoe__DOT__board[2]),2);
	vcdp->chgBus  (c+36,(vlTOPp->tictactoe__DOT__board[3]),2);
	vcdp->chgBus  (c+37,(vlTOPp->tictactoe__DOT__board[4]),2);
	vcdp->chgBus  (c+38,(vlTOPp->tictactoe__DOT__board[5]),2);
	vcdp->chgBus  (c+39,(vlTOPp->tictactoe__DOT__board[6]),2);
	vcdp->chgBus  (c+40,(vlTOPp->tictactoe__DOT__board[7]),2);
	vcdp->chgBus  (c+41,(vlTOPp->tictactoe__DOT__board[8]),2);
	vcdp->chgBus  (c+42,(vlTOPp->tictactoe__DOT__board[9]),2);
	vcdp->chgBus  (c+43,(vlTOPp->tictactoe__DOT__board[10]),2);
	vcdp->chgBus  (c+44,(vlTOPp->tictactoe__DOT__board[11]),2);
	vcdp->chgBus  (c+45,(vlTOPp->tictactoe__DOT__board[12]),2);
	vcdp->chgBus  (c+46,(vlTOPp->tictactoe__DOT__board[13]),2);
	vcdp->chgBus  (c+47,(vlTOPp->tictactoe__DOT__board[14]),2);
	vcdp->chgBus  (c+48,(vlTOPp->tictactoe__DOT__board[15]),2);
	vcdp->chgBit  (c+49,(vlTOPp->tictactoe__DOT__won_condition));
	vcdp->chgBus  (c+50,(vlTOPp->tictactoe__DOT__winner),2);
    }
}

void Vtictactoe::traceChgThis__6(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+51,(vlTOPp->tictactoe__DOT__flip_ram),4);
	vcdp->chgBus  (c+52,(vlTOPp->tictactoe__DOT__board_pos),4);
    }
}

void Vtictactoe::traceChgThis__7(Vtictactoe__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vtictactoe* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBit  (c+53,(vlTOPp->rst));
	vcdp->chgBit  (c+54,(vlTOPp->clk));
	vcdp->chgBit  (c+55,(vlTOPp->hsync));
	vcdp->chgBit  (c+56,(vlTOPp->vsync));
	vcdp->chgBus  (c+57,(vlTOPp->red),3);
	vcdp->chgBus  (c+58,(vlTOPp->green),3);
	vcdp->chgBus  (c+59,(vlTOPp->blue),2);
    }
}
